from asyncio import tasks
from collections import UserString
from crypt import methods
from itertools import product
from flask import Flask, render_template, session, url_for, request, redirect, g, json
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///main.db'
app.config['SQLALCHEMY_BINDS'] = {'user': 'sqlite:///user.db'}
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = 'False' # to supress warning
app.secret_key = '1234'



db = SQLAlchemy(app)
# https://www.youtube.com/watch?v=SB5BfYYpXjE

@app.before_request
def before_request():
    if 'user' in session:
        session['user'] = session['user']
    else:
        g.user = LocalUser("", "", False, False, False)
    
class ModelEncoder(json.JSONEncoder):
    def default(self, o: any) -> any:
        if hasattr(o, 'to_json'):
            return o.to_json()
        else:
            return super(ModelEncoder, self).default(o)

app.json_encoder = ModelEncoder


# definição de uma product
class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    value = db.Column(db.Integer, default=0)
    description = db.Column(db.String(1000), nullable=False)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)
    creator = db.Column(db.String(128), nullable=False)

    def __repr__(self):
        return '<produto %r>' % self.id

# definição de uma user
class User(db.Model):
    login = db.Column(db.String(96), primary_key=True)
    name = db.Column(db.String(157), nullable=False)
    client = db.Column(db.Boolean, nullable=False)
    seller = db.Column(db.Boolean, nullable=False)
    manager = db.Column(db.Boolean, nullable=False)

    def __repr__(self):
        return '<usuario %r>' % self.login

class LocalUser():
    login:str
    name:str
    client:bool
    seller:bool
    manager:bool

    def __init__(self, login, name, client, seller, manager):
        self.login = login
        self.name = name
        self.client = client
        self.seller = seller
        self.manager = manager

    def __repr__(self):
        return '<usuario %r>' % self.login

    def to_json(self):
        return {"login": self.login,
                "name": self.name,
                "client": self.client,
                "seller": self.client,
                "manager": self.manager}


# tela principal
@app.route('/')
def index():
        return redirect('/login/')
    

# tela login
@app.route('/login/', methods=['POST', 'GET'])
def login():
    session['user'] = LocalUser("", "", False, False, False)
    if request.method == 'POST':
        user = User.query.filter_by(login=request.form['login']).first()
        if not user:
            return render_template("login.html", mensagem="Usuario não cadastrado", user = session['user'])
        else:
            session['user'] = LocalUser(user.login, user.name, user.client, user.seller, user.manager)
            return redirect("/vendedor/")
    else:
        return render_template("login.html",mensagem="", user = session['user'])

# tela registro
@app.route('/register/', methods=['POST', 'GET'])
def registro():
    if request.method == 'POST':
        user_login = request.form['login']
        user_name = request.form['name']
        try:
            if request.form['client'] == 'on':
                user_is_client= True
        except:
            user_is_client= False

        try:
            if request.form['seller'] == 'on':
                user_is_seller = True
        except:
            user_is_seller = False
        try:
            if request.form['manager'] == 'on':
                user_is_manager = True
        except:
            user_is_manager = False

        new_user = User(login=user_login, name=user_name, client=user_is_client, seller=user_is_seller, manager=user_is_manager)

        try:
            db.session.add(new_user)
            db.session.commit()
            return render_template("register.html", mensagem = "Usuario Cadastrado com sucesso!", user = session['user'])
        except:
            return 'Ouve um erro ao adicionar usuário'
    else:
        return render_template("register.html", user = session['user'])

# tela vendedor
@app.route('/vendedor/', methods=['POST', 'GET'])
def vendedor():
    if request.method == 'POST':
        product_name = request.form['name']
        product_description = request.form['description']
        product_value = request.form['value']
        product_owner:str = session['user']['login']
        new_product = Product(name=product_name, description=product_description, value=product_value, creator=product_owner)
        
        try:
            db.session.add(new_product)
            db.session.commit()
            return redirect('/vendedor/')
        except:
            return 'Ouve um erro ao adicionar'
    else:
        products = Product.query.filter_by(creator=session['user']['login']).order_by(Product.date_created).all()
        return render_template("vendedor.html", products=products, user = session['user'])


# funcao de deletar (vendedor)
@app.route('/vendedor/delete/<int:id>')
def delete_v(id):
    product_to_delete = Product.query.get_or_404(id)

    try:
        db.session.delete(product_to_delete)
        db.session.commit()
        print ("a")
        return redirect('/vendedor/')
    except:
        return 'Houve um problema ao deletar o produto'


# funcao de atualziar (vendedor)
@app.route('/vendedor/update/<int:id>', methods=['GET', 'POST'])
def update_v(id):
    product = Product.query.get_or_404(id)

    if request.method == 'POST':
        product.name = request.form['name']
        product.description = request.form['description']
        product.value = request.form['value']

        try:
            db.session.commit()
            return redirect('/vendedor/')
        except:
            return 'Houve um erro ao atualziar'
    else:
        return render_template('update_product.html', product = product, user = session['user'])

if __name__ == "__main__":
    app.run(debug=True, port=5002)