# Simple Web Market

A simple web market in the "Mercado Livre" style to exercice web development, using Python with Flask.

## Usage
To use this web application you must have all the requisites fom "requisitos.txt" installed, you can do it via pip

After that, you can just run:

```console
python3 client.py
```

```console
python3 manager.py
```

```console
python3 seller.py
```

To open the interface for the client, the manager and the seller respectively.

### Extra

The UI is not the focus of this application neither security or even usabily, it´s main purpose is to practice web development and try to implement a web application using the Services oriented architecture.
