from asyncio import tasks
from collections import UserString
from crypt import methods
from itertools import product
from flask import Flask, render_template, session, url_for, request, redirect, g, json
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///main.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = 'False' # to supress warning
app.secret_key = '1234'

db = SQLAlchemy(app)

@app.before_request
def before_request():
    if 'user' in session:
        session['user'] = session['user']
    else:
        g.user = LocalUser("", "", False, False, False)
    
class ModelEncoder(json.JSONEncoder):
    def default(self, o: any) -> any:
        if hasattr(o, 'to_json'):
            return o.to_json()
        else:
            return super(ModelEncoder, self).default(o)

app.json_encoder = ModelEncoder


# definição de uma product
class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    value = db.Column(db.Integer, default=0)
    description = db.Column(db.String(1000), nullable=False)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)
    creator = db.Column(db.String(128), nullable=False)

    def __repr__(self):
        return '<produto %r>' % self.id

# definição de uma user
class User(db.Model):
    login = db.Column(db.String(96), primary_key=True)
    name = db.Column(db.String(157), nullable=False)
    client = db.Column(db.Boolean, nullable=False)
    seller = db.Column(db.Boolean, nullable=False)
    manager = db.Column(db.Boolean, nullable=False)

    def __repr__(self):
        return '<usuario %r>' % self.login

class LocalUser():
    login:str
    name:str
    client:bool
    seller:bool
    manager:bool

    def __init__(self, login, name, client, seller, manager):
        self.login = login
        self.name = name
        self.client = client
        self.seller = seller
        self.manager = manager

    def __repr__(self):
        return '<usuario %r>' % self.login

    def to_json(self):
        return {"login": self.login,
                "name": self.name,
                "client": self.client,
                "seller": self.client,
                "manager": self.manager}


# tela principal
@app.route('/')
def index():
    return redirect('/login/')
    

# tela login
@app.route('/login/', methods=['POST', 'GET'])
def login():
    session['user'] = LocalUser("", "", False, False, False)
    if request.method == 'POST':
        user = User.query.filter_by(login=request.form['login']).first()
        if not user:
            return render_template("login.html", mensagem="Usuario não cadastrado", user = session['user'])
        else:
            session['user'] = LocalUser(user.login, user.name, user.client, user.seller, user.manager)
            return redirect("/cliente/")
    else:
        return render_template("login.html",mensagem="", user = session['user'])

# tela registro
@app.route('/register/', methods=['POST', 'GET'])
def registro():
    if request.method == 'POST':
        user_login = request.form['login']
        user_name = request.form['name']
        try:
            if request.form['client'] == 'on':
                user_is_client= True
        except:
            user_is_client= False

        try:
            if request.form['seller'] == 'on':
                user_is_seller = True
        except:
            user_is_seller = False
        try:
            if request.form['manager'] == 'on':
                user_is_manager = True
        except:
            user_is_manager = False

        new_user = User(login=user_login, name=user_name, client=user_is_client, seller=user_is_seller, manager=user_is_manager)

        try:
            db.session.add(new_user)
            db.session.commit()
            return render_template("register.html", mensagem = "Usuario Cadastrado com sucesso!", user = session['user'])
        except:
            return 'Ouve um erro ao adicionar usuário'
    else:
        return render_template("register.html", user = session['user'])

# tela cliente
@app.route('/cliente/', methods=['GET'])
def cliente():
    products = Product.query.order_by(Product.date_created).all()
    return render_template("cliente.html", products=products, user = session['user'])

# funcao de vizualizar 
@app.route('/cliente/view/<int:id>', methods=['GET', 'POST'])
def view(id):
    product = Product.query.get_or_404(id)
    return render_template('view.html', product = product, user = session['user'])



if __name__ == "__main__":
    app.run(debug=True, port=5000)